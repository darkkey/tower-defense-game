#include "CGame.h"

CGame::CTileType::CTileType(SDL_Texture* textureFile, int id, bool isBuildable){
	tileId = id;
	texture = textureFile;
	buildable = isBuildable;
}
int CGame::CTileType::getId(){
	return tileId;
}
SDL_Texture* CGame::CTileType::getTexture(){
	return texture;
}
bool CGame::CTileType::getIsBuildable(){
	return buildable;
}