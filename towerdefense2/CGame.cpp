#include "CGame.h"

CGame::CGame(){
	SDL_Init(SDL_INIT_EVERYTHING);
	TTF_Init();
	tileCount = 10;
	SDL_Color red = {255, 0, 0};
	screenWidth = 1024;
	screenHeight = 768;
	int gameScreenWidth = (screenWidth/4)*3;
	tileWidth = gameScreenWidth/tileCount;
	tileHeight = screenHeight/tileCount;
	window	= SDL_CreateWindow("Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenWidth, screenHeight, SDL_WINDOW_SHOWN | SDL_RENDERER_PRESENTVSYNC);
	render = SDL_CreateRenderer(window, 0, SDL_RENDERER_ACCELERATED);
	if(render == 0){
	  std::cout << "ERROR: Cannot create SDL_Render! SDL_GetError: " << SDL_GetError();
	  running = false;
	}
	over = IMG_LoadTexture(render, "over.png");
	if(over == NULL){
		std::cout << "ERROR: Cannot open over.png file!" << std::endl;
		running = false;
	}
	sentry = IMG_LoadTexture(render, "sentry1.png");
	running	= true;
	font = TTF_OpenFont("font1.ttf", 12);
	SDL_Color tempColor = {0,255,255};
	white = &tempColor;
	message = TTF_RenderText_Solid(font, "Tower Defense Alpha .001", tempColor);
	if(message == NULL){
	  std::cout << "ERROR: Unable to create message! SDL_GetError: " << SDL_GetError() << std::endl;
	  running = false;
	}
	messageTex = SDL_CreateTextureFromSurface(render, message);
	if(messageTex == NULL){
	  std::cout << "ERROR: Unable to create messageTex! SDL_GetError: " << SDL_GetError() << std::endl;
	  running = false;
	}
	designMode = false;
	std::string temp;
	mapFile.open("map.txt", std::ifstream::in);
	if(mapFile.is_open() == 0){
		std::cout << "ERROR: Cannot open map.txt file!" << std::endl;
		running = false;
	}
	nodeFile.open("nodetypes.txt", std::ifstream::in);
	if(nodeFile.is_open() == 0){
		std::cout << "ERROR: Cannot open nodetypes.txt file!" << std::endl;
		running = false;
	}
	if(running){
		loadTypes();
		drawTiles();
	}
	startclock = SDL_GetTicks();
	while(running){
		renderer();
	}

}
void CGame::loadTypes(){
	numTypes = 0;
	std::string tempTexture;
	bool tempIsBuildable = true;
	nodeFile >> numTypes;
	for(int i = 0; i < numTypes; i++){
		nodeFile >> tempTexture;
		nodeFile >> tempIsBuildable;
		const CTileType temp(IMG_LoadTexture(render, tempTexture.c_str()), i, tempIsBuildable);
		tileTypes.push_back(temp);
		std::cout << "Loaded type with id of " << i << " and texture " << tempTexture << std::endl;
	}
}
//
// I fixed that code. Enjoy. :D
//
int CGame::getMouseLocation(int x, int y){
	if(x >= tileHeight*tileCount){
		return -1; //Out of map area
	}else{
		return (y/tileHeight*tileCount)+(x/tileWidth);
	}
}

void CGame::drawTiles(){
	int x = 0;
	int y = 0;
	int counter = 1;
	int counter2 = 1;
	int temp = 0;
	for(int i = 0; i < tileCount*tileCount; i++){
		mapFile >> temp;
		if(temp <= numTypes){
		const CTile temp_tile(x, y, temp, tileWidth, tileHeight, tileTypes[temp].getTexture());
		tiles.push_back(temp_tile);
		}
		else{
			std::cout << "ERROR: Tile Type out of range!" << std::endl;
		}
		x = x + tileWidth;
		if(counter == tileCount){
			counter = 0;
			y = y + tileHeight;
			x = 0;
		}
		counter++;
		counter2++;
		std::cout << "Loaded tile at " << i << " with id of " << temp << std::endl;
	}
}
void CGame::checkInput(){
	SDL_Event e;
	while(SDL_PollEvent( &e ) != 0){
		if(e.type == SDL_QUIT){
			running = false;
		}
		if(e.type == SDL_MOUSEBUTTONDOWN){
			switch(e.button.button){
			case SDL_BUTTON_LEFT:
				if(tempTileSelector != -1 && tileTypes[tiles[tempTileSelector].getType()].getIsBuildable() != 0 && tiles[tempTileSelector].isOccupied == 0 && designMode){
					CSentry temp(1, tempTileSelector, sentry);
					tiles[tempTileSelector].isOccupied = true;
					sentries.push_back(temp);
				}
			break;
			case SDL_BUTTON_RIGHT:
				if(tempTileSelector != -1 && tileTypes[tiles[tempTileSelector].getType()].getIsBuildable() != 0 && tiles[tempTileSelector].isOccupied == 1 && designMode && sentries.size() > 0){
					tiles[tempTileSelector].isOccupied = true;
					unsigned int temp = 0;
					for(unsigned int i = 0; i < sentries.size(); i++){
					  if(sentries[i].getPos() == tempTileSelector){
					    temp = i;
					  }
					}
					sentries.erase(sentries.begin() + temp);
					tiles[tempTileSelector].isOccupied = false;
					std::cout << "That would remove sentry with pos of " << temp << std::endl;
				}
			break;
			}
		}
		if(e.type == SDL_KEYDOWN){
			switch(e.key.keysym.sym){
			case SDLK_TAB:
				designMode = !designMode;
			break;
			case SDLK_ESCAPE:
				running = false;
			break;
			}
		}
	}
}
void CGame::renderer(){
	deltaclock = SDL_GetTicks() - startclock;
	startclock = SDL_GetTicks();
	if ( deltaclock != 0 )
		currentFPS = 1000 / deltaclock;
	SDL_RenderClear(render);
	for(unsigned int i = 0; i < tiles.size(); i++){ // unsigned int i in order to suppress signed/unsigned mismatch warning.
		SDL_Rect temp = tiles[i].getBox();
		SDL_RenderCopy(render, tiles[i].getTexture(), NULL, &temp);
	}
	for(unsigned int i = 0; i < sentries.size(); i++){
		SDL_Rect temp = tiles[sentries[i].getPos()].getBox();
		SDL_RenderCopy(render, sentries[i].getTexture(), NULL, &temp);
	}
	checkInput();
	SDL_Rect Message_rect; //create a rect
	Message_rect.x = 0;  //controls the rect's x coordinate
	Message_rect.y = 0; // controls the rect's y coordinte
	Message_rect.w = 18*14; // controls the width of the rect
	Message_rect.h = 17; // controls the height of the rect
	SDL_RenderCopy(render, messageTex, NULL, &Message_rect);
	SDL_GetMouseState(&tempX, &tempY);
	tempTileSelector = getMouseLocation(tempX, tempY);
	if(tempTileSelector != -1){
		if(tileTypes[tiles[tempTileSelector].getType()].getIsBuildable() != 0){
		SDL_Rect temp = tiles[tempTileSelector].getBox();
		SDL_RenderCopy(render, over, NULL, &temp);
		}
	}
	SDL_Delay(16);
	SDL_RenderPresent(render);
	std::stringstream title;
	title.str("");
	title << "Current FPS: " << currentFPS;
	SDL_SetWindowTitle(window, title.str().c_str());
}
