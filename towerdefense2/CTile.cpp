#include "CGame.h"

CGame::CTile::CTile(int x, int y, int tileType, int tileWidth, int tileHeight, SDL_Texture* nodeTexture){
	box.x = x;
	box.y = y;

	box.h = tileHeight;
	box.w = tileWidth;

	type = tileType;

	texture = nodeTexture;
	
	isOccupied = false;
}

SDL_Rect CGame::CTile::getBox(){
	return box; 
}

SDL_Texture* CGame::CTile::getTexture(){
	return texture;
}

int CGame::CTile::getType(){
	return type;
}
