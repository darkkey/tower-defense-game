#include "CGame.h"
CGame::CSentry::CSentry(int sentryType, int sentryPos, SDL_Texture* sentryTexture){
	type = sentryType;
	pos = sentryPos;
	texture = sentryTexture;
}
SDL_Texture* CGame::CSentry::getTexture(){
	return texture;
}
int CGame::CSentry::getPos(){
	return pos;
}
