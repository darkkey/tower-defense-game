#ifndef CGAME_H_INCLUDED
#define CGAME_H_INCLUDED

#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <string>

class CGame{
public:
	CGame();
private:
	class CTile{
	public:
		CTile(int x, int y, int tileType, int tileHeight, int tileWidth, SDL_Texture* nodeTexture);
		int getType();
		SDL_Rect getBox();
		SDL_Texture* getTexture();
		bool isOccupied;
	private:
		SDL_Rect box;
		int type;
		SDL_Texture* texture;
	};
	class CTileType{
	public:
		CTileType(SDL_Texture* textureFile, int id, bool isBuildable);
		int getId();
		SDL_Texture* getTexture();
		bool getIsBuildable();
	private:
		int tileId;
		SDL_Texture* texture;
		bool buildable;
	};
	class CSentry{
	public:
		CSentry(int sentryType, int sentryPos, SDL_Texture* sentryTexture);
		int getPos();
		SDL_Texture* getTexture();
		int getType();
	private:
		int					pos;
		SDL_Texture*		texture;
		int					type;
	};
	std::ifstream			mapFile;
	std::ifstream			nodeFile;
	bool					running;
	SDL_Surface*			windowSurface;
	SDL_Surface				scoreSurface;
	SDL_Renderer*			render;
	SDL_Window*				window;
	SDL_Texture*			texture;
	SDL_Texture*			over;
	SDL_Texture*			sentry;
	TTF_Font*				font;
	SDL_Color*				white;
	SDL_Surface*				message;
	SDL_Texture* 				messageTex;
	std::vector<CTile>		tiles;
	std::vector<CTileType>	tileTypes;
	std::vector<CSentry>	sentries;
	void drawTiles();
	void renderer();
	void checkInput();
	void loadTypes();
	int getMouseLocation(int y, int x);
	int						screenWidth;
	int						screenHeight;
	int						tileWidth;
	int						tileHeight;
	int						numTypes;
	int						tempX;
	int						tempY;
	int						tileCount;
	int						tempTileSelector;
	bool					designMode;
	//FPS Counting
	Uint32 startclock = 0;
	Uint32 deltaclock = 0;
	Uint32 currentFPS = 0;

};

#endif
